﻿using System;
using System.Text;
using System.Collections.Generic;

namespace cssorganizer
{
    enum LineStates
    {
        GROUPING, RULING
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please input css: ");
            string line = "";
            var cssRaw = new LinkedList<string>(); 
            while (line != ":q") 
            {
                if (line != "") // Empty string is null...
                    cssRaw.AddLast(line);
                line = Console.ReadLine(); 
            }
            var organizer = new Organizer(cssRaw);
            string css = organizer.GetOrganizedCSS();
            Console.WriteLine("New CSS:");
            Console.WriteLine("-------------------------------------"); 
            Console.Write(css);
            Console.WriteLine("-------------------------------------");
        }
    }
}
