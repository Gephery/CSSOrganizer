﻿using System;
using System.Collections.Generic;
using System.Text; 
namespace cssorganizer
{
    public class Organizer
    {

        private Dictionary<string, SortedSet<PairComp>> ruleToGroup;
        private SortedDictionary<PairComp, SortedSet<string>> groupingToRules; 

        public Organizer(LinkedList<string> lineByLineCss)
        {
            this.InitRuleToGroup(lineByLineCss);
            this.InitGroupingToRules();
        }

        public string GetOrganizedCSS()
        {
            StringBuilder cssBuilder = new StringBuilder();

            var it = this.groupingToRules.GetEnumerator(); 
            while (it.MoveNext())
            {
                cssBuilder.Append(it.Current.Key.GetEl());
                cssBuilder.Append(" {\n"); 
                foreach (string rule in it.Current.Value)
                {
                    cssBuilder.Append("    ");
                    cssBuilder.Append(rule);
                    cssBuilder.Append('\n'); 
                }
                cssBuilder.Append("}\n\n"); 
            }

            return cssBuilder.ToString(); 
        }

        private void InitGroupingToRules()
        {
            this.groupingToRules = new SortedDictionary<PairComp, SortedSet<string>>();

            var it = this.ruleToGroup.GetEnumerator();
            StringBuilder groupBuilder = new StringBuilder(); 
            while (it.MoveNext())
            {
                foreach (var temp in it.Current.Value) {
                    groupBuilder.Append(temp.GetEl());
                    groupBuilder.Append(", ");
                }
                groupBuilder.Remove(groupBuilder.Length - 2, 1);
                string group = groupBuilder.ToString();
                groupBuilder.Clear();
                var groupPair = new PairComp(group.Trim(new char[] { '.', '#' }), group); 
                if (!this.groupingToRules.TryGetValue(groupPair, out SortedSet<string> tempRules))
                {
                    tempRules = new SortedSet<string>();
                    this.groupingToRules.Add(groupPair, tempRules); 
                }
                tempRules.Add(it.Current.Key); // Add that css rule 
            }
        }

        private void InitRuleToGroup(LinkedList<string> lineByLineCss)
        {
            this.ruleToGroup = new Dictionary<string, SortedSet<PairComp>>(); 
            var it = lineByLineCss.GetEnumerator();
            while (it.MoveNext())
            {
                string cssLine = it.Current;
                // TODO check if can do contains from back to front 
                if (cssLine.Contains('{'))
                {
                    string groupStr = cssLine.Trim(new char[] { ' ', '{' });
                    // New grouping
                    string[] groups = groupStr.Split(new char[] { ','}, StringSplitOptions.RemoveEmptyEntries);

                    it.MoveNext();
                    // Assumes well formed css so should not reach end before `}`
                    // also } must be on own line 
                    while (!it.Current.Contains('}'))
                    {
                        cssLine = it.Current;
                        // TODO maybe change so works for tab too...
                        string cleanRule = cssLine.Trim(new char[] { ' ', '\t' });
                        if (!this.ruleToGroup.TryGetValue(cleanRule, out SortedSet<PairComp> tempGroup))
                        {
                            tempGroup = new SortedSet<PairComp>();
                            this.ruleToGroup.Add(cleanRule, tempGroup);
                        }
                        foreach (string group in groups)
                        {
                            tempGroup.Add(new PairComp(group.Trim(new char[] {' ', '.', '#'}), 
                                                                           group.Trim(new char[] { ' ' })));
                        }
                        it.MoveNext();
                    }
                }
            }
        }

        private class PairComp : IComparable<PairComp>
        {
            private KeyValuePair<string, string> el;

            public PairComp(string key, string value) 
            {
                this.el = new KeyValuePair<string, string>(key, value); 
            }

            public string GetEl()
            {
                return this.el.Value; 
            }

            public int CompareTo(PairComp other)
            {
                return this.el.Key.CompareTo(other.el.Key); 
            }
        }
    }
}
